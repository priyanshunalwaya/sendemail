import {
    LightningElement,
    track
} from 'lwc';
import sendEmail from '@salesforce/apex/EmailClass.sendEmail';
import {
    ShowToastEvent
} from 'lightning/platformShowToastEvent';

export default class SendEmail extends LightningElement {
    @track obj = {};
    @track showSpinner = false;
    handleChange(event) {
        this.obj[event.target.name] = event.target.value;
    }

    isValidFields() {
        let isAllValid = [
            ...this.template.querySelectorAll("lightning-input")
        ].reduce((validSoFar, input) => {
            if (input && input.value) input.value = input.value.trim();
            input.reportValidity();
            return validSoFar && input.checkValidity();
        }, true);
        console.log(isAllValid);
        return isAllValid;
    }

    sendEmail() {
        this.showSpinner = true;
        if (this.isValidFields()) {
            if (this.obj.Body != '' && this.obj.subject != '' && this.obj.receiverAddress != '') {


                sendEmail({
                        jsonData: JSON.stringify(this.obj)
                    })
                    .then(result => {
                        if (result == 'Specify the receiver and body for the email') {
                            this.toastEvent(result, 'info');
                        } else {
                            this.toastEvent(result, 'success');
                        }
                        this.showSpinner = false;
                    })
                    .catch(error => {
                        console.log(JSON.stringify(error));
                        this.showSpinner = false;
                    });
            }
        } else {
            this.showSpinner = false;
            this.toastEvent('Enter valid values', 'warning');
        }
    }

    toastEvent(result, variantType) {
        const event = new ShowToastEvent({
            title: result,
            variant: variantType,
        });
        this.dispatchEvent(event);
    }
}