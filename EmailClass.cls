public with sharing class EmailClass {
    /*
    @author : Priyanshu Nalwaya
    @Description : Calling this method to send email
    @param : emailBody, subject , receiveraddress(if multiple then separate with ','), senderName, ccTo
    @return : returns a string
    */
    @AuraEnabled
    public static string sendEmail(String jsonData){
        EmailWrapper wrapper = (EmailWrapper) JSON.deserialize(jsonData , EmailWrapper.class); 
        if ( String.isBlank(wrapper.receiveraddress) || String.isBlank(wrapper.Body)) {
            return 'Specify the receiver and body for the email';
        }

        List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();

        Messaging.SingleEmailMessage mail =  new Messaging.SingleEmailMessage();
        // Step 1: list of people who should get the email
        List<String> receiverEmailList = wrapper.receiveraddress.split(',');
        mail.setToAddresses(receiverEmailList);
        // Step 3: Set who the email is sent from
        mail.setSenderDisplayName(wrapper.senderName);
        // Set list of people who should be CC'ed(optional)
        if(String.isNotBlank(wrapper.ccAddress)) {
            List<String> ccAddress = wrapper.ccAddress.split(','); 
            mail.setCcAddresses(ccAddress);
        }
        // Step 4. Set email contents - you can use variables!
        mail.setSubject(wrapper.subject);
        mail.setHtmlBody(wrapper.Body);

        // Step 5. Add your email to the master list
        mails.add(mail);
        // Step 6: Send all emails in the master list
        Messaging.sendEmail(mails);
        return 'mail send successfully';
    }
    public class EmailWrapper{
        public string Body;
        public string subject;
        public string ccAddress;
        public string receiveraddress;
        public string senderName;

    }
}